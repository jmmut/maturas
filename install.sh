#!/bin/bash
#
# -first you must decompress, if you didn't already:
#tar -zxvf release.tar.gz
#
# -next enter into the directory
#cd release*
#
# you can build the program with make, 
# the file is in the src directory

cd src
make
cd ..

# finally, to launch:
#./visor
