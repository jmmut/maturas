#ifndef _PLOT_H_
#define _PLOT_H_

typedef struct
{
	float x_tam;
	float y_tam;	// this height will be full red
	float z_tam;
} PlotConfig;

class Plot
{
	public:
		PlotConfig config;	// virtual size, display purposes only
		float *map;	//  = new float [x*z]
		int x;	// actual number of elements in the array 
		int z;

		// if you call this constructor you have to call Init later
		Plot(){map = NULL;}

		// recommended constructor 
		Plot(int rows_z, int columns_x, PlotConfig configp)
		{
			map = NULL;
			Init(rows_z, columns_x, configp);
		};

		void Init(int rows_z, int columns_x, PlotConfig configp = {10, 1, 10})
		{
			x = columns_x;
			z = rows_z;
			config = configp;
			if (map != NULL)
				delete map;
			map = new float[x*z];
		}

		void set(float * matrix)
		{
			if (map != NULL)
				delete map;
			map = matrix;
		}

		~Plot() { delete map; }

		// returned value is asignable
		float& operator()(int elem) { return map[elem]; }

		/**
		 * this assumes that the matrix is row-major order,
		 * with z in rows and x in columns. succesive x are contiguous.
		 * thus, recommended access is for z { for x { plot(z, x) } }
		 */
		float& operator()(int row_z, int column_x) { return map[row_z*x + column_x]; }	// inline?
};

#endif //_PLOT_H_
