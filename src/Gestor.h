#ifndef _GESTOR_H_
#define _GESTOR_H_

#include "vgm/Ventana.h"
#include "vgm/ShaderManager.h"
#include "Memoria.h"


class Gestor: public Ventana, public Memoria, public ShaderManager
{
	public:
		Gestor(int width = 640, int height = 480);
		virtual ~Gestor();
		int init();
		void initGL();
		void onStep(float);
		void onDisplay();
		void onKeyPressed(SDL_Event &e);
		void onPressed(const Pulsado &p);
		void onMouseMotion(SDL_Event &e);
		void onMouseButton(SDL_Event &e);

		void cogerTerreno(Plot& plot);
		void generarTerreno(float (*f)(float x, float z), Plot& plot);
		//void construirTerreno();
	private:
		void generarTextura(Plot& plot);
		void setupVertices(Plot &plot);

		void drawAxes();
		int errors;
};
#endif
