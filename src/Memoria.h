#ifndef _MEMORIA_H_
#define _MEMORIA_H_

#include <SDL.h>
#include <complex>
#include <random>
#include "math/Vector3D.h"
#include "graphics/drawables/Volador.h"
#include "Plot.h"
//#include "Objeto.h"

struct Memoria
{
	int last_click_x, last_click_y;
	int xant, yant;
	int t;
	int precision_mapa_x, precision_mapa_z;
	Volador vldr;
	float vel;
	bool avanzar;
	float drot;
	float dtrl;
	float ctrl;
	float escala;
	PlotConfig config;
	Plot *terreno;
	GLfloat *vertices;
	unsigned int vao;
	float point_size;
	GLuint alturas;
	//Objeto bola;
};


#endif
