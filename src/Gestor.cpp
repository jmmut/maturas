#include "Gestor.h"

/**
 * TODO
 *	glBindFragDataLocation
 *	glDrawBuffers
 */
Gestor::Gestor(int width, int height): Ventana(width, height)
{
	errors = 0;
	t = 0;
	point_size = 2;

	srand(time(NULL));

	setFps(55);
	
	semaforoDisplay.abrir();
	semaforoStep.abrir();

	initGL();
	if (!initShaders()) {
		errors |= 0x1;
	}

	if (!loadShaders("./shaders/deforma.vs.glsl", "./shaders/deforma.fs.glsl")) {
		errors |= 0x2;
	}

	vel = 1;
	vldr.setVel(vel);
	vldr.setOrientacion(Vector3D(1, 0, 0).Unit(), Vector3D(0, 1, 0));
	vldr.setPos(Vector3D(0, 2, 0));

	avanzar = false;
	drot = 2;
	dtrl = 0.1;
	ctrl = 1.05;
	vertices = NULL;
	vao = 0;
}
Gestor::~Gestor()
{
	if (errors) {
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "The next errors occurred during Gestor's lifetime:\n");
	}
	if (errors & 0x1) {
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "initShaders returned false: there's no shader support\n");
	}
	if (errors & 0x2) {
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "loadShaders returned false: it is likely that the path is wrong, or they didn't compile\n");
	}
	if (vertices != NULL)
		delete vertices;
}



/** A general OpenGL initialization function.
 * Sets all of the initial parameters.
 * We call this right after our OpenGL window is created.
 */
void Gestor::initGL()
{
	GLdouble aspect;
	int width = 640, height = 480;

	if (window == NULL)
		SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION
				, "There's no window, GL will use (640, 480) in the viewport, and you must create the sdl gl context ");
	else
	{
		SDL_GetWindowSize(window, &width, &height);
		context = SDL_GL_CreateContext(window);
		if (!context) {
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Unable to create OpenGL context: %s\n", SDL_GetError());
			SDL_Quit();
			exit(2);
		}
	}

	glViewport(0, 0, width, height);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);        // This Will Clear The Background Color To Black
	glClearDepth(1.0);                // Enables Clearing Of The Depth Buffer
	glDepthFunc(GL_LESS);                // The Type Of Depth Test To Do
	glEnable(GL_DEPTH_TEST);            // Enables Depth Testing
	glShadeModel(GL_SMOOTH);            // Enables Smooth Color Shading

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();                // Reset The Projection Matrix

	aspect = (GLdouble)width / height;

	perspectiveGL (45, aspect, 0.1, 100);

	//glOrtho(-1.0, 1.0, -1.0 / aspect, 1.0 / aspect, 0.0, 1.0);

	glMatrixMode(GL_MODELVIEW);

	glPointSize(point_size);
	glDisable(GL_POINT_SMOOTH);

}
/**
 * Copies the height map as a texture accesible from the shader.
 * It is assumed that the elements in the height map are contiguous:
 * {(z=0, x=0), (z=0, x=1), ..., (z=0, x=x-1), (z=1, x=0), ..., (z=z-1, x=x-1)}
 */
void Gestor::generarTextura(Plot &plot)
{
	glGenTextures(1, &alturas);
	glBindTexture(GL_TEXTURE_2D, alturas);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, plot.x, plot.z, 0, GL_RED, GL_FLOAT, plot.map);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}

void Gestor::setupVertices(Plot &plot)
{
	int n = plot.x*plot.z;
	int iz, ix;
	int elem = 0;
	float x_inc = plot.config.x_tam/plot.x;
	float z_inc = plot.config.z_tam/plot.z;
	vertices = new float[3*n];	// (x, y, z)*num_vertices
	for (iz = 0; iz < plot.z; iz++)
	{
		for (ix = 0; ix < plot.x; ix++)
		{
			vertices[elem++] = ix*x_inc;
			vertices[elem++] = 0;
			vertices[elem++] = iz*z_inc;
		}
	}
	unsigned int vbo = 0;
	glGenBuffers (1, &vbo);
	glBindBuffer (GL_ARRAY_BUFFER, vbo);
	glBufferData (GL_ARRAY_BUFFER, 3 * n * sizeof (float), vertices, GL_STATIC_DRAW);

	glGenVertexArrays (1, &vao);
	glBindVertexArray (vao);
	glEnableVertexAttribArray (0);
	glBindBuffer (GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
	/*
	 * //old version
	glBegin(GL_POINTS);

	for (int i_z = 0; i_z < precision_mapa_z; i_z++)
	{
		for (int i_x = 0; i_x < precision_mapa_x; i_x++)
		{
			glVertex3f(i_x*config.x_tam/precision_mapa_x, 0, i_z*config.z_tam/precision_mapa_z);
		}
	}
	glEnd();
	*/
}

void Gestor::cogerTerreno(Plot &plot)
{
	precision_mapa_x = plot.x;
	precision_mapa_z = plot.z;
	config = plot.config;
	terreno = &plot;
	vel = sqrt(config.z_tam*config.x_tam);
	vldr.setVel(vel);
	vldr.setPos(Vector3D(0, 2, 0));

	//for (int i = 0; i < x_size*z_size; i++)
	//{
	////cout << "height_map[" << i << "] = " << height_map[i] << endl;	// DEPURACION
	//}

	setupVertices(plot);
	generarTextura(plot);
}

/**
 * Calls the function f with values in [0, 1) with size_x points in the x axis,
 * and size_z points in z axis.
 */
void Gestor::generarTerreno(float (*f)(float x, float z), Plot &plot)
{
	int iz, ix;
	for (iz = 0; iz < plot.z; iz++)
	{
		for (ix = 0; ix < plot.x; ix++)
		{
			plot(iz, ix) = f(float(ix)/plot.x, float(iz)/plot.z);
		}
	}

	cogerTerreno(plot);
}


/**
 * ampliacion en horizontal + error
 *//*
	  void GestorTerr::ConstruirTerreno()
	  {
	  precision_mapa_x = 500;
	  precision_mapa_z = 500;
	  size = 10;
	  max_alt = 1;
	  escala = 1.0/(precision_mapa_x/size);
	  vel = size/2;
	  vldr.setVel(vel);
	  vldr.setPos(Vector3D(0, 2*max_alt, 0));

	  point = new float[precision_mapa_x*precision_mapa_z];
	  int i, j;


	  std::random_device rd;
	  std::mt19937 gen(rd());
	  std::normal_distribution<> d(0, max_alt/50);	// distribucion normal(media, desviacion estandar)

	  point[0] = max_alt/2 + d(gen);	// primer elemento de la primera fila

	  for (i = 1; i < precision_mapa_x; i++)	// primera fila
	  {
	  point[i] = point[i-1] + d(gen);
//if (i >= 50 &&  i< 55)
//point[i] = -1;
}

for (i = 1; i < precision_mapa_z; i++)	// segunda y el resto de filas
{
point[i*precision_mapa_z] = point[(i-1)*precision_mapa_z] + d(gen);
for (j = 1; j < precision_mapa_x; j++)	// segundo elemento y el resto de cada fila
{
//point[i*precision_mapa + j] = (point[(i-1)*precision_mapa + j] + point[i*precision_mapa + j-1])/2.0 + d(gen);
point[i*precision_mapa_z + j] = 
(
//point[
// point[(i-1)*precision_mapa + j-1] 
//+
point[(i-1)*precision_mapa_z + j]
+ point[i*precision_mapa_z + j-1]
+ d(gen)
)/2 
;// + d(gen);
}
}

GenerarTextura(point, precision_mapa_x, precision_mapa_z);
}*/


void Gestor::onStep(float dt)
{
	t++;
	if (avanzar)
		vldr.Avanza(dt);
	
}

void Gestor::drawAxes()
{
	glPushMatrix();
	glBegin(GL_LINES);
	glColor3f(1, 0, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(100, 0, 0);

	glColor3f(0, 1, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 100, 0);

	glColor3f(0, 0, 1);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 0, 100);
	glEnd();

	glBegin(GL_TRIANGLES);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 2, 0);
	glVertex3f(0, 2, 1);
	glEnd();
	glPopMatrix();
}

void Gestor::onDisplay()
{
	GLint location;
	// Set the background black
	glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
	// Clear The Screen And The Depth Buffer
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glLoadIdentity();                // Reset The View
	//glRotatef(240, 0, 0, 1);	// weirdest error ever. de repente esto se hizo necesario. ???
	vldr.Look();
	drawAxes();

	glPushMatrix();
	selectShader(0);
	setUniform("max_alt", config.y_tam);
	setUniform("x_size", config.x_tam);
	setUniform("z_size", config.z_tam);

	location = glGetUniformLocationARB(shaders[currentShader].program, "t");
	if (location >= 0) {
		glUniform1iARB(location, t);
	}

	glBindVertexArray (vao);
	glDrawArrays(GL_POINTS, 0, terreno->x*terreno->z);
	unselectShader();
	glPopMatrix();

	SDL_GL_SwapWindow(window);
}

//void Gestor::OnPressed(const Pulsado &p)
//{
//cout << "p.sym = " << p.sym << endl;	// DEPURACION
//}

void Gestor::onKeyPressed(SDL_Event &e)
{
	int entero;

	if (e.type == SDL_KEYUP)
		return;

	switch(e.key.keysym.sym)
	{
		case SDLK_p:	// play / stop
			if (semaforoDisplay.estado())
			{
				semaforoStep.cerrar();
				semaforoDisplay.cerrar();
				semaforoDisplay.sumar();
			}
			else
			{
				semaforoStep.abrir();
				semaforoDisplay.abrir();
			}
			break;
			//case SDLK_H:
			//cout << "Hola" << endl;
			//
			//break;
		case SDLK_h:
			//SDL_LOG();
			cout << "Ayuda:\n\t"
				"r: redraw\n\t"
				"c: cuadros por segundo (FPS)\n\t"
				"\n";	// TODO print_file("ayuda.txt");
			break;
		case SDLK_SPACE:
			avanzar = !avanzar;
			break;
		case SDLK_m:
			vel *= -1;
			vldr.setVel(vel);
			break;
		case SDLK_c:
			cout << "Nuevos FPS: " << endl;
			SDLcin(entero);
			setFps(entero);
			break;
		case SDLK_g:
			cout << "vldr.getPos() = " << vldr.getPos() << endl;	// DEPURACION
			cout << "vldr.getDir() = " << vldr.getDir() << endl;	// DEPURACION
			cout << "vldr.getUp() = " << vldr.getUp() << endl;	// DEPURACION
			break;
		case SDLK_f:
			{
				string fich;
				ofstream f;

				cout << "fichero de salida (/dev/null para cancelar): " << endl;
				cout << "recomendable que el nombre contenga las dimensiones para no olvidarlas (p.e. XXX-" <<
					terreno->z << "-" << terreno->x << ".txt" << endl;
				cin >> fich;
				f.open(fich.c_str());
				if (!f)
				{
					SDL_LogError(SDL_LOG_CATEGORY_ERROR, "no se pudo abrir fichero %s", fich.c_str());
					return;
				}

				for (int i = 0; i < terreno->x*terreno->z; i++)
				{
					f << (*terreno)(i) << " ";
				}
				f.close();
			}
			break;
		default:
			break;
	}
}

void Gestor::onPressed(const Pulsado &p)
{
	switch(p.sym)
	{
		case SDLK_u:
			//vldr.setPos(vldr.getPos() + Vector3D(0, dtrl, 0));
			vldr.rotatef(-drot, 0, 0, 1);
			break;
		case SDLK_o:
			//vldr.setPos(vldr.getPos() + Vector3D(0, -dtrl, 0));
			vldr.rotatef(drot, 0, 0, 1);
			break;
		case SDLK_i:
			//vldr.setPos(vldr.getPos() + Vector3D(0, dtrl, 0));
			vldr.rotatef(drot, 1, 0, 0);
			break;
		case SDLK_k:
			//vldr.setPos(vldr.getPos() + Vector3D(0, -dtrl, 0));
			vldr.rotatef(-drot, 1, 0, 0);
			break;
		case SDLK_l:
			vldr.rotY(-drot);
			break;
		case SDLK_j:
			vldr.rotY(drot);
			break;
		case SDLK_e:
			vldr.setPos(vldr.getPos() + vldr.getDir()*dtrl);
			break;
		case SDLK_q:
			vldr.setPos(vldr.getPos() - vldr.getDir()*dtrl);
			break;
		case SDLK_w:
			vldr.setPos(vldr.getPos() + vldr.getUp()*dtrl);
			break;
		case SDLK_s:
			vldr.setPos(vldr.getPos() - vldr.getUp()*dtrl);
			break;
		case SDLK_d:
			vldr.setPos(vldr.getPos() - vldr.getX()*dtrl);
			break;
		case SDLK_a:
			vldr.setPos(vldr.getPos() + vldr.getX()*dtrl);
			break;
		case SDLK_KP_MINUS:
		case SDLK_MINUS:
			vel = vldr.getVelMod()/ctrl;
			vldr.setVel(vel);
			break;
		case SDLK_KP_PLUS:
		case SDLK_PLUS:
			vel = vldr.getVelMod()*ctrl;
			vldr.setVel(vel);
			break;
	}
}

void Gestor::onMouseButton(SDL_Event &e)
{
	last_click_x = e.button.x;
	last_click_y = e.button.y;

	semaforoDisplay.sumar();
}

void Gestor::onMouseMotion(SDL_Event &e)
{
	xant = e.button.x;
	yant = e.button.y;

	//semaforoDisplay.sumar();
}





