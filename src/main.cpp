#include "Gestor.h"
/**
 * modo de uso:
 * ./maturas c 2
 * utiliza gestor->CogerTerreno con la funcion terrenos[2]
 * cogerTerreno recibe un plot completo, la funcion terrenos[2] debe generar
 * toda la matriz
 *
 * ./maturas g 1
 * utiliza gestor->GenerarTerreno con la funcion plot[1]
 * GenerarTerreno va pidiendo valores a plot[1]
 */

float Aleat()
{
	static std::random_device rd;
	static std::mt19937 gen(rd());
	static std::normal_distribution<> d(0, 0.30/50);	// distribucion normal(media, desviacion estandar)

	return d(gen);
}
/**
 * senos compuestos
 */
Plot &SenosGenera(int x, int z)
{
	//float max_alt = 1;
	static Plot plot(z, x, {10, 1, 10});
	int i;
	float parte = 0.5;

	for (i = 0; i < x*z; i++)
	{
		plot(i) = 
			(sin(float(i/x)*8/z*2*M_PI)+1)*0.5*parte
			+ (sin(i%x/float(x)*2*M_PI)+1)*0.5*parte
			;
		//cout << "point[" << i << "] = " << point[i] << endl;	// DEPURACION
	}

	cout << " estas en SenosGenera" << endl;
	return plot;
}


/**
 * ampliacion en horizontal + error
 */
Plot& Horiz(int x, int z)
{
	static Plot plot(z, x, {10, 1, 10});
	float max_alt = 1;
	int iz, ix;

	std::random_device rd;
	std::mt19937 gen(rd());
	std::normal_distribution<> d(0, max_alt/50);	// distribucion normal(media, desviacion estandar)

	plot(0, 0) = max_alt/2 + d(gen);	// primer elemento de la primera fila

	for (ix = 1; ix < x; ix++)	// primera fila
	{
		plot(0, ix) = plot(0, ix-1) + d(gen);
	}

	for (iz = 1; iz < z; iz++)	// segunda y el resto de filas
	{
		plot(iz, 0) = plot(iz-1, 0) + d(gen);
		for (ix = 1; ix < x; ix++)	// segundo elemento y el resto de cada fila
		{
			plot(iz, ix) =
				(
				 plot(iz-1, ix)
				 + plot(iz, ix-1)
				)/2 
				+ d(gen)
				;
		}
	}
	return plot;
}
/* version vieja
   Plot& Horiz(int x, int z)
   {
   static Plot plot(x, z, {10, 1, 10});
   float max_alt = 1;

   float *point = plot.map;
   int i, j;

   std::random_device rd;
   std::mt19937 gen(rd());
   std::normal_distribution<> d(0, max_alt/50);	// distribucion normal(media, desviacion estandar)

   point[0] = max_alt/2 + d(gen);	// primer elemento de la primera fila

   for (i = 1; i < x; i++)	// primera fila
   {
   point[i] = point[i-1] + d(gen);
//if (i >= 50 &&  i< 55)
//point[i] = -1;
}

for (i = 1; i < z; i++)	// segunda y el resto de filas
{
point[i*x] = point[(i-1)*x] + d(gen);
for (j = 1; j < x; j++)	// segundo elemento y el resto de cada fila
{
//point[i*precision_mapa + j] = (point[(i-1)*precision_mapa + j] + point[i*precision_mapa + j-1])/2.0 + d(gen);
point[i*x + j] = 
(
//point[
// point[(i-1)*precision_mapa + j-1] 
//+
point[(i-1)*x + j]
+ point[i*x + j-1]
)/2 
+ d(gen)
;// + d(gen);
}
}
return plot;
}
*/
/**
 * ampliacion en derivada + error
 */
float *Deriv(int x, int z)
{
	float max_alt = 1;

	float *point = new float[x*z];
	int i, j;

	//float aleat;

	std::random_device rd;
	std::mt19937 gen(rd());
	std::normal_distribution<> d(0, max_alt/500);	// distribucion normal(media, desviacion estandar)

	point[0] = max_alt/2;	// primer elemento de la primera fila
	//aleat =  + d(gen);
	point[1] = point[0];	// segundo elemento de la primera fila
	//aleat =  + d(gen);

	cout << "0" << endl;
	cout << "1" << endl;
	for (i = 2; i < x; i++)	// primera fila
	{
		point[i] = 2*point[i-1] - point[i-2] + d(gen);
		//if (i >= 50 &&  i< 55)
		//point[i] = -1;
		cout << i << endl;
	}
	point[x] = point[0] + d(gen);	// primer elemento de la segunda fila
	point[x+1] = 
		(
		 point[1]
		 + point[x]
		)/2
		+ d(gen);	// segundo elemento de la segunda fila

	cout << x << endl;
	cout << x+1 << endl;
	for (i = x+2; i < 2*x; i++)	// segunda fila
	{
		point[i] = ( point[i-x] + /* parte horizontal del eje z */ 2*point[i-1] - point[i-2] /* parte derivada del eje x*/)/2 + d(gen);
		//if (i >= 50 &&  i< 55)
		//point[i] = -1;
		cout << i << endl;
	}

	for (i = 2; i < z; i++)	// tercera y el resto de filas
	{
		point[i*x] = 2*point[(i-1)*x] - point[(i-2)*x] + d(gen);	// primer elemento
		point[i*x+1] = 
			(
			 2*point[(i-1)*x] - point[(i-2)*x] // parte derivada del eje z
			 + point[i-1] // parte horizontal del eje x
			)/2
			+ d(gen);	// segundo elemento
		cout << i*x << endl;
		cout << i*x+1 << endl;
		for (j = 2; j < x; j++)	// tercer elemento y el resto de cada fila
		{
			//point[i*precision_mapa + j] = (point[(i-1)*precision_mapa + j] + point[i*precision_mapa + j-1])/2.0 + d(gen);
			point[i*x + j] = 
				(
				 2*point[(i-1)*x + j] - point[(i-2)*x + j] // parte derivada del eje z
				 + 2*point[i*x + j-1] - point[i*x + j-2] // parte derivada del eje x
				)/2 
				+ d(gen);
			cout << i*x+j << endl;
		}
	}
	return point;
}



void Rec (Plot &p, int ix0, int ix2, int iz0, int iz2, int lvl)
{
	if (ix0+1 == ix2 && iz0+1 == iz2)
	{
		return; 
	}

	int ix1 = (ix0+ix2)/2 , iz1 = (iz0+iz2)/2;
	float escala = (ix2-ix1);

	p(iz2, ix1) = (p(iz2, ix0) + p(iz2, ix2))/2 + Aleat()*escala;
	p(iz1, ix2) = (p(iz0, ix2) + p(iz2, ix2))/2 + Aleat()*escala;
	p(iz1, ix1) = (p(iz1, ix0) + p(iz1, ix2) + p(iz0, ix1) + p(iz2, ix1))/4 + Aleat()*escala;

	Rec(p, ix0, ix1, iz0, iz1, lvl+1);
	Rec(p, ix1, ix2, iz0, iz1, lvl+1);
	Rec(p, ix0, ix1, iz1, iz2, lvl+1);
	Rec(p, ix1, ix2, iz1, iz2, lvl+1);
}

/**
 * requires squared plots
 * the pattern of modification given Os is Xs:
 * O-O
 * -XX
 * OXO
 */
Plot &Iter (int x, int z)
{
	static Plot plot(z, x, {20, 2, 20});
	int ix0 = 0
		, ix1 = x-1
		, iz0 = 0
		, iz1 = z-1;
	float valor = plot.config.y_tam/2.0;
	plot(iz0, ix0) = valor;
	plot(iz1, ix1) = valor;

	for (int ix = 1; ix < x; ix++)
	{
		plot(iz0, ix) = valor;
	}
	for (int iz = 1; iz < z; iz++)
	{
		plot(iz, ix0) = valor;
	}

	if(x != pow(2,floorf(log2f(x)))+1 || x != z)
	{
		cout << "las dimensiones no sirven para el algoritmo Rec" << endl;
		cout << "x = " << x << endl;	// DEPURACION
		cout << "z = " << z << endl;	// DEPURACION
	}
	else
		Rec(plot, ix0, ix1, iz0, iz1, 0);

	return plot;
}

float Ave (float x, float z)
{
	return fabs(x*x-z*z)*5;
}

float SenosPlot (float x, float z)
{
	float parte = 0.7;
	return 0.5+0.5*
		(parte*sin(x*2*M_PI)
		 + (1-parte)*sin(8*z*2*M_PI));
}

float Paraboloide (float x, float z)
{
	x = 2*x - 1; 
	z = 2*z - 1; 
	return (x*z+1)*2.5;
}

float Recurs (float x, float z)
{
	if (x < 0.0625 && z < 0.0625)
	{
		return Paraboloide(x, z);
	}

	return +Recurs (x/2, z/2) - Paraboloide(x, z);
}

float Cupula (float x, float z)
{
	x = 2*x - 1; 
	z = 2*z - 1; 
	return (-x*x -z*z +2)*2.5; 
}

int main(int argc, char **argv)
{
	//int f = 0;
	Gestor *gestor = new Gestor(1200, 600);
	int z = 513, x = 513;
	bool defecto = false;

	vector<float (*)(float, float)> plots = {
		Ave,
		Paraboloide,
		Recurs,
		Cupula
	};
	vector<Plot& (*)(int, int)> terrenos = {
		SenosGenera,
		Horiz,
		Iter
	};
	unsigned int num_func;
	PlotConfig config{10, 5, 10};
	Plot plot(x, z, config);
	float faux;

	if (argc >= 3)
	{
		num_func = atoi(argv[2]);
		switch (argv[1][0])
		{
			case 'g':
				if (num_func < plots.size())
					gestor->generarTerreno(plots[num_func], plot);
				else
				{
					cout << "la funcion que pides no está: hay de la 0 a la " << plots.size()-1 << endl;
					defecto = true;
				}
				break;
			case 'c':
				if (num_func < terrenos.size())
				{
					gestor->cogerTerreno(terrenos[num_func](x, z));
				}
				else
				{
					cout << "la funcion que pides no está: hay de la 0 a la " << terrenos.size()-1 << endl;
					defecto = true;
				}
				break;
			case 'i':
				z = atoi(argv[2]);
				x = atoi(argv[3]);
				plot.Init(z, x);
				for (int i = 0; i < x*z; i++)
				{
					cin >> faux;
					plot(i) = faux;
				}
				gestor->cogerTerreno(plot);
				break;
			default:
				defecto = true;
		}
	}
	else
		defecto = true;

	if (defecto)
	{
		cout << "modo de uso:" << endl <<
			"./maturas c 2" << endl <<
			"utiliza gestor->CogerTerreno con la funcion terrenos[2]" << endl <<
			"cogerTerreno recibe un plot completo, la funcion terrenos[2] debe generar" << endl <<
			"toda la matriz" << endl <<
			"" << endl <<
			"./maturas g 1" << endl <<
			"utiliza gestor->GenerarTerreno con la funcion plot[1]" << endl <<
			"GenerarTerreno va pidiendo valores a plot[1]" << endl <<
			"" << endl <<
			"./maturas i 256 256" << endl <<
			"utiliza la entrada estandar para cargar una matriz ya creada (filas, columnas)" << endl <<
			"elementos de cada fila contiguos: (0,0),(0,1),(0,2)..." << endl;
		gestor->generarTerreno(Ave, plot);
	}

	gestor->mainLoop();

	delete gestor;
	return 0;
}

