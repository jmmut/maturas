varying vec4 v_pos;
uniform float x_size;
uniform float z_size;
uniform float max_alt;
uniform sampler2D alturas;

void main()
{
	vec4 color = vec4(1.0, 1.0, 0.0, 1.0);

	color.g = 1.0-texture2D(alturas, vec2(v_pos.x/x_size, v_pos.z/z_size)).x/max_alt;	// note the final .x because we made an "only red" texture
	//color.g = 1-v_pos.y/max_alt;
	gl_FragColor = color;
}

