varying vec4 v_pos;
uniform float x_size;
uniform float z_size;
uniform float max_alt;
uniform sampler2D alturas;
uniform int t;

void main()
{
	vec4 pos = gl_Vertex;
	pos.y += texture2D(alturas, vec2(pos.x/x_size, pos.z/z_size)).x;	// note the final .x because we made an "only red" texture
	//pos.y += sin(t/100.0)/5*pos.y;
	//pos.y *= sin(t/100.0);
	gl_Position = gl_ModelViewProjectionMatrix * pos;

	v_pos = pos;
}

